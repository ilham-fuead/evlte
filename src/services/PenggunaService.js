import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost/evapi',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
  },
  withCredentials: false,
})

export default {
  getPenggunaInfo() {
    return apiClient.get('/api/pengguna/senarai_pengguna_info.php')
  },
  getPenggunaData(currentPage) {
    return apiClient.get('/api/pengguna/senarai_pengguna_data.php?currentPage=' + currentPage)
  }
}
