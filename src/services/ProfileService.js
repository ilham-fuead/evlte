import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost/evapi',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
  },
  withCredentials: false,
})

export default {
  getProfile() {
    return apiClient.get('/profile.php')
  },
}
